# -*- coding: utf-8 -*-

from News_crawler.data_env.env import DataEnv

class DataWriter(DataEnv):
    def __init__(self, args):
        super(DataWriter, self).__init__(args)
            
     #-----------------------------------------------------------------------
    def insertOneData(self, dbName, tableName, data_dict, index_field):
        flt = {index_field: data_dict[index_field]}
        #pdb.set_trace()
        self._dbClient[dbName][tableName].update_one(flt, {'$set':data_dict}, upsert = True)

class Params(object):
    def __init__(self):
        # launch mongod server before use mongo or pymongo to connect the database 
        self.database_host = 'localhost'
        self.database_port = 27017
        
if __name__ == "__main__":
    args = Params()
    interface = DataWriter(args)