from setuptools import setup

setup(name='News_crawler',
      version='1.0',
      description='Crawl stock news from sina and xueqiu',
      url='',
      author='yahui',
      author_email='yahui.liang@hotmail.com',
      license='MJL',
      packages=['News_crawler'],
      install_requires=[
        "pandas",
        "pymongo",
        	"requests",
        	"selenium",
        	"bs4"],
      zip_safe=False)
