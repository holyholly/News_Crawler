# -*- coding: utf-8 -*-

from News_crawler.data_env.data_reader import DataReader, Params
from datetime import datetime
import pymongo
import pdb

def read_xueqiu_company_news(dbName, symbol, start, end, projections, sort_by = None, limit = 10):
    args = Params()
    args.database_host = 'localhost'
    args.database_port = 27017
    reader = DataReader(args)
    flt = {'created_datetime': {'$gte':datetime.strptime(start, "%Y%m%d"),'$lte':datetime.strptime(end, "%Y%m%d")}}
    if sort_by is None:
        data_df = reader.fetchData(dbName, symbol, flt, projections, 'DataFrame')
    else:
        sort_list = [[i, pymongo.DESCENDING] for i in sort_by]
        data_df = reader.fetchSortedData(dbName, symbol, flt, projections, sort_list, limit, 'DataFrame')
    return data_df


if __name__ == "__main__":
    projections = ["title", "symbol", "created_datetime", "retweet_count", "reply_count", "fav_count", "link"]
    sort_by = ['fav_count','reply_count','retweet_count']
    data_df = read_xueqiu_company_news('xueqiu_company_news', 'SZ000001','20180101','20180329', projections, sort_by)
