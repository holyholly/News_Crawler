# -*- coding: utf-8 -*-

from News_crawler.data_env.env import DataEnv
import pdb
from pandas import DataFrame

class DataReader(DataEnv):
    def __init__(self, args):
        super(DataReader, self).__init__(args)
        
    def fetchData(self, dbName, tbName, filter_condition, projections, dataType='list'):
        fields={'_id':0}
        list(map(lambda x:fields.update({x:1}), projections))
        cl = self._dbClient[dbName][tbName]
        data = list(cl.find(filter=filter_condition, projection=fields))
        if len(data) > 0:
            if dataType == 'DataFrame':
                data = DataFrame(data)
            return data
        else:
            return None
    
    def fetchSortedData(self, dbName, tbName, filter_condition, projections, sort_by, limit, dataType='list'):
        fields={'_id':0}
        list(map(lambda x:fields.update({x:1}), projections))
        cl = self._dbClient[dbName][tbName]
        data = list(cl.find(filter=filter_condition, projection=fields).sort(sort_by).limit(limit))
        if len(data) > 0:
            if dataType == 'DataFrame':
                data = DataFrame(data)
            return data
        else:
            return None
        
    def fetchAllData(self, dbName, tbName, dataType='list'):
        cl = self._dbClient[dbName][tbName]
        data = list(cl.find())
        if len(data) > 0:
            if dataType == 'DataFrame':
                data = DataFrame(data)
                data = data.drop('_id', axis=1)
            return data
        else:
            return None
    
    def getCollections(self, dbName):
        db = self._dbClient[dbName]
        col_names = list(db.collection_names())
        return col_names
    
    def getCollectionKeys(self, dbName, tbName):
        cl = self._dbClient[dbName][tbName]
        keys = cl.find_one().keys()
        return keys
    
class Params(object):
    def __init__(self):
        # launch mongod server before use mongo or pymongo to connect the database 
        self.database_host = 'localhost'
        self.database_port = 27017