# -*- coding: utf-8 -*-

import pymongo
from pymongo.errors import ConnectionFailure

class DataEnv(object):
    def __init__(self, args):
        #self.logger = args.logger
        self.host = args.database_host
        self.port = args.database_port
        #other login params
        if hasattr(args,'authSource'):
            self.authSource = args.authSource
        else:
            self.authSource = ''
            
        if hasattr(args, 'username') and hasattr(args, 'password'):
            self.username = args.username
            self.password = args.password
            self.uri = "mongodb://{username}:{password}@{host}:{port}/{authSource}".format(username=self.username,
                                                                                           password = self.password,
                                                                                           host = self.host,
                                                                                           port = self.port,
                                                                                           authSource = self.authSource)
        self.loginDatabase()        
    #------------------------------------------------------------------------
    def loginDatabase(self):
        """登录mongodb database"""
        try:
            if hasattr(self,'uri'):
                self._dbClient = pymongo.MongoClient(self.uri)
            else:
                self._dbClient = pymongo.MongoClient(self.host, self.port)
        except ConnectionFailure:
            print("mongodb Server not available")