# -*- coding: utf-8 -*-

from News_crawler.lib import xueqiu_company_news_crawl as xcnc
from News_crawler.lib import get_cookie
    
def download_crawl_results(symbol_list, dbName):
    cookie_list = get_cookie.GetCookie('https://xueqiu.com', 3)
    for symbol in symbol_list:
        print("downloading news for {}".format(symbol))
        xcnc.crawl_company_news(symbol, cookie_list, dbName)

if __name__ == "__main__":
    symbol_list = ['000001','000002']
    download_crawl_results(symbol_list, 'xueqiu_company_news')