# -*- coding: utf-8 -*-

class CompanyNews(object):
    
    def __init__(self):
        self.id = None
        self.title = None
        self.symbol = None
        self.created_datetime = None
        self.retweet_count = None
        self.reply_count = None
        self.fav_count = None
        self.link = None